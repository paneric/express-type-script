import AssocArray from './AssocArray';

export default class DI
{
    private definitions: AssocArray;
    private container: AssocArray;

    constructor()
    {
        this.container = new AssocArray();
        this.definitions = new AssocArray();
    }

    public get(key: string): any
    {
        if (this.has(key)) {
            return this.getInstance(key);
        }

        return undefined;
    }

    public has(key: string): boolean
    {
        if (this.definitions[key] !== undefined) {
            return true;
        }

        return false;
    }

    public set(key: string, object: any): void
    {
        this.definitions[key] = true;
        this.container[key] = object;
    }

    public setDefinitions(definitions: AssocArray): void
    {
        this.definitions = definitions;
    }

    private getInstance(key: string)
    {
        if (this.container[key] !== undefined) {
            return this.container[key];
        }

        this.container[key] = this.definitions[key](this);

        return this.container[key];
    }
}
