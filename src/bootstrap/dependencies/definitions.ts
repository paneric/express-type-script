import AssocArray from '../../System/AssocArray';
import controllersDefinitions from './controller';
import DI from '../../System/DI';

export default function (di: DI): AssocArray
{
    let definitions: AssocArray = new AssocArray();

    definitions = controllersDefinitions(di, definitions);

    return definitions;
}
