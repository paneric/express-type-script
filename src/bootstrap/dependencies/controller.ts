import AssocArray from '../../System/AssocArray';
import DI from '../../System/DI';
import { MessageController } from '../../Controller/ChatController';

export default function (di: DI, definitions: AssocArray): AssocArray
{
    definitions['message_controller'] = () => new MessageController();

    return definitions;
}
