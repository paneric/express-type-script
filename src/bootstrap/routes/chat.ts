import { Request, Response, Router } from "express";
import DI from '../../System/DI';
import { MessageController } from '../../Controller/ChatController';

export default function (di: DI, router: Router): Router
{
    router.get('/chat/add', (req: Request, res: Response) => {
        const controller: MessageController = di.get('message_controller');
        return controller.add(req, res);
    });

    return router;
}
