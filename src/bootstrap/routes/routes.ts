import DI from '../../System/DI';
import { Router } from "express";
import express from 'express';
import chatRoutes from './chat';

export default function (di: DI): Router
{
    let router = express.Router();

    router = chatRoutes(di, router);

    return router;
}
