import express from 'express';
import DI from './System/DI';
import definitions from './bootstrap/dependencies/definitions';
import routes from './bootstrap/routes/routes';

const app = express();

const di: DI = new DI();
di.setDefinitions(definitions(di));

app.use('/', routes(di));

const PORT = process.env.PORT || 8080;

app.listen(PORT, () => console.log(`Server started on port ${PORT}`));
